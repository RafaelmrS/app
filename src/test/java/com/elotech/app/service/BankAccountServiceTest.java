package com.elotech.app.service;

import com.elotech.app.entity.BankAccount;
import com.elotech.app.entity.Person.Person;
import com.elotech.app.enumeration.TypeAccount;
import com.elotech.app.repository.BankAccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.*;



@RunWith(MockitoJUnitRunner.class) //Ver depois melhor pra que serve isso

public class BankAccountServiceTest {

    @Mock //Ver melhor
    private BankAccountRepository repository;

    @InjectMocks //?????????
    private BankAccountService service;

    @Test
    public void deveRetornarEntidadePorNumeroDaConta(){
        //Construção
        BankAccount bankAccount = createEntidade();

        when(repository.findByAccountNumber("123123-12")).thenReturn(bankAccount);

        //Execução do método
        BankAccount bankAccountRetornada = service.buscaContaPeloAccountNumber("123123-12");

        //Verificação dos dados

        assertThat(bankAccountRetornada).isEqualTo(bankAccount);
        assertThat(bankAccountRetornada.getAccountNumber()).isEqualTo("123123-12");

    }

    private BankAccount createEntidade() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAgency("1L");
        bankAccount.setDescription("Conta do Jayme");
        bankAccount.setId(1L);
        bankAccount.setTypeAccount(TypeAccount.CURRENT);
        bankAccount.setPerson(new Person());
        bankAccount.setAccountNumber("123123-12");

        return bankAccount;
    }
}