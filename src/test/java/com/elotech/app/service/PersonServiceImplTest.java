package com.elotech.app.service;

import com.elotech.app.dto.PersonDTO;
import com.elotech.app.dto.PersonFilterDTO;
import com.elotech.app.dto.UpdateAddressDTO;
import com.elotech.app.entity.Person.Address;
import com.elotech.app.entity.Person.ContactPerson;
import com.elotech.app.entity.Person.Person;
import com.elotech.app.enumeration.TypeResidence;
import com.elotech.app.repository.PersonRepository;
import com.elotech.app.service.impl.PersonServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class PersonServiceImplTest {

    @Mock
    private PersonRepository personRepositoryMock;

    @InjectMocks
    private PersonServiceImpl personService;

    @Test
    public void shouldGetPersonById() {
        final var person = Person
                .builder()
                .contactPerson(List.of())
                .address(Address.builder().number(123).build())
                .dateOfBirth(LocalDate.of(2020, 02, 02))
                .name("Test")
                .rg("123123123")
                .build();
        person.setId(1L);

        given(personRepositoryMock.getPersonById(1L))
                .willReturn(person);

        final var result = personService.getPersonById(1L);

        assertThat(result).isEqualTo(person);

        verify(personRepositoryMock).getPersonById(1L);
    }

    @Test
    public void shouldGetPersonPage() {
        final var filterDto =
                PersonFilterDTO
                        .builder()
                        .page(0)
                        .size(15)
                        .orderBy("dateOfBirth")
                        .order(Sort.Direction.ASC)
                        .filter("name")
                        .build();

        final var pageBlanc = new PageImpl<Person>(List.of());

        given(personRepositoryMock.getPagePerson(filterDto))
                .willReturn(pageBlanc);

        final var result = personService.getPagePerson(filterDto);

        assertThat(result).isEqualTo(pageBlanc);

        verify(personRepositoryMock)
                .getPagePerson(filterDto);
    }

    @Test
    public void shouldRegisterPerson() {
        final var contactPerson = ContactPerson
                .builder()
                .email("email@email.com")
                .build();
        final var address = Address
                .builder()
                .address("Test")
                .build();
        final var person = PersonDTO
                .builder()
                .contactPerson(List.of(contactPerson))
                .address(address)
                .dateOfBirth(LocalDate.of(2020, 02, 02))
                .name("Test")
                .rg("123123123")
                .build();

        doNothing().when(personRepositoryMock).savePerson(any());

        personService.registerPerson(person);

        final var personArgumentCaptor = ArgumentCaptor.forClass(Person.class);

        verify(personRepositoryMock).savePerson(personArgumentCaptor.capture());

        assertThat(personArgumentCaptor.getValue().getRg()).isEqualTo(person.getRg());
        assertThat(personArgumentCaptor.getValue().getAddress().getAddress()).isEqualTo(address.getAddress());
        assertThat(personArgumentCaptor.getValue().getContactPerson().get(0).getEmail()).isEqualTo(contactPerson.getEmail());

    }

    @Test
    public void shouldUpdateAddress() {
        final var address = Address
                .builder()
                .address("Test Update")
                .typeResidence(TypeResidence.BUILDING)
                .build();
        final var updateAddress = UpdateAddressDTO
                .builder()
                .address(address)
                .build();
        final var person = Person
                .builder()
                .contactPerson(List.of())
                .address(Address.builder().address("Test").typeResidence(TypeResidence.HOUSE).build())
                .dateOfBirth(LocalDate.of(2020, 02, 02))
                .name("Test")
                .rg("123123123")
                .build();
        person.setId(1L);

        given(personRepositoryMock.getPersonById(1L))
                .willReturn(person);

        doNothing()
                .when(personRepositoryMock).savePerson(any());

        personService.updateAddress(1L, updateAddress);

        final var personArgumentCaptor = ArgumentCaptor.forClass(Person.class);

        verify(personRepositoryMock).savePerson(personArgumentCaptor.capture());

        assertThat(personArgumentCaptor.getValue().getAddress().getAddress()).isEqualTo("Test Update");
        assertThat(personArgumentCaptor.getValue().getAddress().getTypeResidence()).isEqualTo(TypeResidence.BUILDING);
    }

    @Test
    public void shouldDeletePerson() {
        final var person = Person.builder().build();
        person.setId(1L);

        doNothing().when(personRepositoryMock).deletePerson(person.getId());

        personService.deletePerson(person.getId());

        final var personArgumentCaptor = ArgumentCaptor.forClass(Long.class);

        verify(personRepositoryMock).deletePerson(personArgumentCaptor.capture());

        assertThat(personArgumentCaptor.getValue()).isEqualTo(person.getId());
    }
}
