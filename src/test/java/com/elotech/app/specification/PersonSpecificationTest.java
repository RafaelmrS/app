package com.elotech.app.specification;

import com.elotech.app.AbstractSpecificationTest;
import org.junit.Test;
import org.mockito.BDDMockito;

import static com.elotech.app.specification.PersonSpecification.filter;
import static org.mockito.BDDMockito.*;

public class PersonSpecificationTest extends AbstractSpecificationTest {

    @Test
    public void testFilter() {
        final var specificationMock = invokeMocked(() -> filter("key", "value"));

        verify(specificationMock.root).get("key");
        verify(specificationMock.cb).equal(any(), eq("value"));
    }

    @Test
    public void testFilterEmpty() {
        invokeAndExpectDefaultInvocation(value -> filter(null, value), null, "");
    }
}
