package com.elotech.app.controller;

import com.elotech.app.dto.PersonDTO;
import com.elotech.app.dto.PersonFilterDTO;
import com.elotech.app.dto.UpdateAddressDTO;
import com.elotech.app.entity.Person.Address;
import com.elotech.app.entity.Person.ContactPerson;
import com.elotech.app.entity.Person.Person;
import com.elotech.app.enumeration.TypeResidence;
import com.elotech.app.service.impl.PersonServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;


import java.time.LocalDate;
import java.util.List;

import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
@ActiveProfiles("test")
public class PersonControllerTest {

    @MockBean
    private PersonServiceImpl personService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Gson gson;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        gson = new GsonBuilder().serializeNulls().create();

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void shouldGetPersonById() throws Exception {
        final var person = Person
                .builder()
                .contactPerson(List.of())
                .address(Address.builder().number(123).build())
                .dateOfBirth(LocalDate.of(2020, 02, 02))
                .name("Test")
                .rg("123123123")
                .build();
        person.setId(1L);

        given(personService.getPersonById(1L))
                .willReturn(person);

        this.mockMvc.perform(get("/api/v1/person/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        verify(personService).getPersonById(1L);

    }

    @Test
    public void shouldGetPageOfPerson() throws Exception {
        final var person = List.of(Person
                .builder()
                .contactPerson(List.of())
                .address(Address.builder().number(123).build())
                .dateOfBirth(LocalDate.of(2020, 02, 02))
                .name("Test")
                .rg("123123123")
                .build());
        person.get(0).setId(1L);

        given(personService.getPagePerson(any()))
                .willReturn(new PageImpl<>(person));

        mockMvc
                .perform(get("/api/v1/person")
                        .param("page", "0")
                        .param("size", "15")
                        .param("orderBy", "name")
                        .param("order", "ASC")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.size()", is(1)));

        final var argumentCaptor = ArgumentCaptor.forClass(PersonFilterDTO.class);

        verify(personService).getPagePerson(argumentCaptor.capture());

        final var personFilterDTO = argumentCaptor.getValue();

        assertThat(personFilterDTO.getPage()).isEqualTo(0);
        assertThat(personFilterDTO.getOrder()).isEqualTo(Sort.Direction.ASC);
        assertThat(personFilterDTO.getSize()).isEqualTo(15);
        assertThat(personFilterDTO.getOrderBy()).isEqualTo("name");
    }

    @Test
    public void shouldRegisterPerson() throws Exception {
        final var contactPerson = ContactPerson
                .builder()
                .email("email@email.com")
                .build();
        final var address = Address
                .builder()
                .address("Test")
                .build();
        final var person = PersonDTO
                .builder()
                .contactPerson(List.of(contactPerson))
                .address(address)
                .name("Test")
                .rg("123123123")
                .build();

        doNothing().when(personService).registerPerson(any());

        this.mockMvc
                .perform(post("/api/v1/person")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(gson.toJson(person))
                )
                .andDo(print())
                .andExpect(status().isNoContent());

        verify(personService).registerPerson(any());
    }

    @Test
    public void shouldUpdateAddress() throws Exception {
        final var address = Address
                .builder()
                .address("Test Update")
                .typeResidence(TypeResidence.BUILDING)
                .build();
        final var updateAddress = UpdateAddressDTO
                .builder()
                .address(address)
                .build();

        doNothing().when(personService).updateAddress(1L, updateAddress);

        this.mockMvc
                .perform(put("/api/v1/person/1")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(gson.toJson(updateAddress))
                )
                .andDo(print())
                .andExpect(status().isNoContent());

        verify(personService).updateAddress(1L, updateAddress);

    }

    @Test
    public void shouldDeletePerson() throws Exception {
        final var person = Person.builder().build();
        person.setId(1L);

        doNothing().when(personService).deletePerson(1L);

        this.mockMvc
                .perform(delete("/api/v1/person/1")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                )
                .andDo(print())
                .andExpect(status().isNoContent());

        verify(personService).deletePerson(1L);

        final var personArgumentCaptor = ArgumentCaptor.forClass(Long.class);

        verify(personService).deletePerson(personArgumentCaptor.capture());

        assertThat(personArgumentCaptor.getValue()).isEqualTo(person.getId());
    }
}
