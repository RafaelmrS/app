package com.elotech.app.repository;

import com.elotech.app.dto.PersonFilterDTO;
import com.elotech.app.entity.Person.Address;
import com.elotech.app.entity.Person.ContactPerson;
import com.elotech.app.entity.Person.Person;
import com.elotech.app.repository.impl.PersonRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.ArgumentMatchers.any;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class PersonRepositoryImplTest {

    @Mock
    private PersonJpaRepository personJpaRepositoryMock;

    @InjectMocks
    private PersonRepositoryImpl personRepository;


    @Test
    public void shouldGetPersonById() {
        final var person = Person
                .builder()
                .contactPerson(List.of())
                .address(Address.builder().number(123).build())
                .dateOfBirth(LocalDate.of(2020,02,02))
                .name("Test")
                .rg("123123123")
                .build();
        person.setId(1L);

        given(personJpaRepositoryMock.findById(1L))
                .willReturn(Optional.of(person));

        final var result = personRepository.getPersonById(person.getId());

        assertThat(result.getName()).isEqualTo("Test");
        assertThat(result.getAddress().getNumber()).isEqualTo(123);
        assertThat(result.getDateOfBirth()).isEqualTo(LocalDate.of(2020,02,02));
        assertThat(result.getRg()).isEqualTo("123123123");

        verify(personJpaRepositoryMock).findById(1L);

    }

    @Test
    public void shouldThrowException() {
        final var person = Person
                .builder()
                .contactPerson(List.of())
                .address(Address.builder().number(123).build())
                .dateOfBirth(LocalDate.of(2020,02,02))
                .name("Test")
                .rg("123123123")
                .build();
        person.setId(1L);

        given(personJpaRepositoryMock.findById(person.getId()))
                .willReturn(any());
        try {
            personRepository.getPersonById(3L);
            fail("Expected exception.");
        }catch (EntityNotFoundException ex) {
            assertThat(ex.getMessage()).isEqualTo("Person not found!");
        }
    }

    @Test
    public void shouldGetPageOfPerson() {
        final var filterDto =
                PersonFilterDTO
                        .builder()
                        .page(0)
                        .size(15)
                        .orderBy("dateOfBirth")
                        .order(Sort.Direction.ASC)
                        .filter("name")
                        .build();

        final var pageBlanc = new PageImpl<Person>(List.of());

        final var sort = Sort.by(
                Sort.Order.asc("name"),
                Sort.Order.by("dateOfBirth").with(Sort.Direction.ASC)
        );
        final var pageable = PageRequest.of(
                0,
                15,
                sort
        );

        given(personJpaRepositoryMock.findAll(any(), refEq(pageable)))
                .willReturn(pageBlanc);

        final var result = personRepository.getPagePerson(filterDto);

        assertThat(result).isEqualTo(pageBlanc);
        final var pageableArgument = ArgumentCaptor.forClass(Pageable.class);
        final var specificationArgument = ArgumentCaptor.forClass(Specification.class);

        verify(personJpaRepositoryMock)
                .findAll(specificationArgument.capture(), pageableArgument.capture());

        assertThat(pageableArgument.getValue()).isEqualTo(pageable);
    }

    @Test
    public void shouldRegisterPerson() {
        final var contactPerson = ContactPerson
                .builder()
                .email("email@email.com")
                .build();
        final var address = Address
                .builder()
                .address("Test")
                .build();
        final var person = Person
                .builder()
                .contactPerson(List.of(contactPerson))
                .address(address)
                .dateOfBirth(LocalDate.of(2020,02,02))
                .name("Test")
                .rg("123123123")
                .build();
        person.setId(1L);

        given(personJpaRepositoryMock.save(person))
                .willReturn(person);

        personRepository.savePerson(person);

        final var personArgumentCaptor = ArgumentCaptor.forClass(Person.class);

        verify(personJpaRepositoryMock).save(personArgumentCaptor.capture());

        assertThat(personArgumentCaptor.getValue().getRg()).isEqualTo(person.getRg());
        assertThat(personArgumentCaptor.getValue().getAddress().getAddress()).isEqualTo(address.getAddress());
        assertThat(personArgumentCaptor.getValue().getContactPerson().get(0).getEmail()).isEqualTo(contactPerson.getEmail());
    }

    @Test
    public void shouldDeletePerson() {
        final var person = Person.builder().build();
        person.setId(1L);

        doNothing().when(personJpaRepositoryMock).deleteById(person.getId());

        personRepository.deletePerson(person.getId());

        final var personArgumentCaptor = ArgumentCaptor.forClass(Long.class);

        verify(personJpaRepositoryMock).deleteById(personArgumentCaptor.capture());

        assertThat(personArgumentCaptor.getValue()).isEqualTo(person.getId());
    }
}
