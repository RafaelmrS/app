package com.elotech.app;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.junit.After;
import org.junit.Before;
import org.springframework.data.jpa.domain.Specification;
import static org.mockito.Answers.RETURNS_MOCKS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public abstract class AbstractSpecificationTest {
    private final ThreadLocal<List<SpecificationMock<?>>> generatedSpecificationMocks =
            ThreadLocal.withInitial(ArrayList::new);

    @Before
    public final void setUp() {
        generatedSpecificationMocks.get().clear();
    }

    @After
    public final void tearDown() {
        for (SpecificationMock<?> specificationMock : generatedSpecificationMocks.get()) {
            verifyNoMoreInteractions(
                    specificationMock.root,
                    specificationMock.query,
                    specificationMock.cb
            );
        }
    }

    protected final <E> SpecificationMock<E> invokeMocked(
            Supplier<Specification<E>> specificationSupplier
    ) {
        @SuppressWarnings("unchecked") final var rootMock = (Root<E>) mock(Root.class, RETURNS_MOCKS);
        final var criteriaQueryMock = mock(CriteriaQuery.class, RETURNS_MOCKS);
        final var criteriaBuilderMock = mock(CriteriaBuilder.class, RETURNS_MOCKS);
        final var specification = specificationSupplier.get();

        specification.toPredicate(rootMock, criteriaQueryMock, criteriaBuilderMock);

        final var specificationMock = new SpecificationMock<>(rootMock, criteriaQueryMock, criteriaBuilderMock);

        generatedSpecificationMocks.get().add(specificationMock);

        return specificationMock;
    }

    @SafeVarargs
    protected final <E, F> void invokeAndExpectDefaultInvocation(
            Function<F, Specification<E>> specificationSupplier,
            F... arguments
    ) {
        for (final var argument : arguments) {
            final var specificationMock = invokeMocked(() -> specificationSupplier.apply(argument));

            verify(specificationMock.cb).and();
        }
    }

    protected static class SpecificationMock<E> {
        public final Root<E> root;
        public final CriteriaQuery<?> query;
        public final CriteriaBuilder cb;

        private SpecificationMock(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            this.root = root;
            this.query = query;
            this.cb = cb;
        }
    }
}
