package com.elotech.app.enumeration;

public enum TypeResidence {
    HOUSE,
    BUILDING,
    APARTMENT
}
