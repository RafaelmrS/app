package com.elotech.app.repository;

import com.elotech.app.entity.Person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PersonJpaRepository extends
        JpaSpecificationExecutor<Person>,
        PagingAndSortingRepository<Person, Long> {
}
