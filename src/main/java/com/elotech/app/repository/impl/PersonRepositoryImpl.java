package com.elotech.app.repository.impl;

import com.elotech.app.dto.PersonFilterDTO;
import com.elotech.app.entity.Person.Person;
import com.elotech.app.repository.PersonJpaRepository;
import com.elotech.app.repository.PersonRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import static com.elotech.app.specification.PersonSpecification.filter;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Repository
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class PersonRepositoryImpl implements PersonRepository {
    private PersonJpaRepository personJpaRepository;

    @Override
    public Person getPersonById(Long personId) {
        return personJpaRepository.findById(personId)
                .stream()
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Person not found!"));
    }

    @Override
    public Page<Person> getPagePerson(PersonFilterDTO personFilterDTO) {
        final var sort = Sort.by(
                Sort.Order.asc("name"),
                Sort.Order.by(personFilterDTO.getOrderBy()).with(personFilterDTO.getOrder())
        );

        final Pageable pageable = PageRequest.of(
                personFilterDTO.getPage(),
                personFilterDTO.getSize(),
                sort
        );

        return personJpaRepository.findAll(
                filter("name" ,personFilterDTO.getFilter()),
                pageable
        );
    }

    @Override
    public void savePerson(Person person) {
        personJpaRepository.save(person);
    }

    @Override
    public void deletePerson(Long personId) {
        personJpaRepository.deleteById(personId);
    }
}
