package com.elotech.app.repository;

import com.elotech.app.dto.PersonFilterDTO;
import com.elotech.app.entity.Person.Person;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PersonRepository {

    Person getPersonById(Long personId);

    Page<Person> getPagePerson(PersonFilterDTO personFilterDTO);

    void savePerson(Person person);

    void deletePerson(Long personId);

}
