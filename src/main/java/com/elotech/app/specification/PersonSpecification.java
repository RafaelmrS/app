package com.elotech.app.specification;

import com.elotech.app.entity.Person.Person;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PersonSpecification {

    public static Specification<Person> filter(String key, String value) {
        if (value == null || value.isEmpty()) {
            return (root, query, cb) -> cb.and();
        }
        return (root, query, cb) -> cb.equal(root.get(key), value);
    }
}
