package com.elotech.app.controller;

import com.elotech.app.dto.PersonDTO;
import com.elotech.app.dto.PersonFilterDTO;
import com.elotech.app.dto.UpdateAddressDTO;
import com.elotech.app.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/person", produces = "application/json")
@AllArgsConstructor
public class PersonController {

    private PersonService personService;


    @GetMapping(path = "/{personId}")
    public ResponseEntity getPersonById(@PathVariable Long personId) {
        return ResponseEntity.ok(personService.getPersonById(personId));
    }

    @GetMapping
    public ResponseEntity getPagePerson(
            @RequestParam(value = "filter", defaultValue = "name") String filter,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "15") int size,
            @RequestParam(value = "orderBy", defaultValue = "name") String orderBy,
            @RequestParam(value = "order", defaultValue = "ASC") Sort.Direction order
    ) {
        final var filterDto = PersonFilterDTO
                .builder()
                .filter(filter)
                .order(order)
                .orderBy(orderBy)
                .page(page)
                .size(size)
                .build();
        final var pagePerson = personService.getPagePerson(filterDto);
        return ResponseEntity.ok(pagePerson.map(PersonDTO::from));
    }

    @PostMapping
    public ResponseEntity registerPerson(@RequestBody PersonDTO personDTO) {
        personService.registerPerson(personDTO);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(path = "/{personId}")
    public ResponseEntity updateAddress(@PathVariable Long personId, @RequestBody UpdateAddressDTO updateAddressDTO) {
        personService.updateAddress(personId, updateAddressDTO);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/{personId}")
    public ResponseEntity deletePerson(@PathVariable Long personId) {
        personService.deletePerson(personId);
        return ResponseEntity.noContent().build();
    }
}
