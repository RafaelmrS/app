package com.elotech.app.controller;

import com.elotech.app.entity.BankAccount;
import com.elotech.app.repository.BankAccountRepository;
import com.elotech.app.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/bank-account", produces = "application/json")
public class BankAccountController {

    @Autowired
    private BankAccountService bankAccountService;

    @PostMapping(path = "/AccountNumber/{account_number}")
    public Optional<BankAccount> searchForAccountNumber(@PathVariable(name = "AccountNumber", required = true) String account_number){
        return Optional.ofNullable(bankAccountService.buscaContaPeloAccountNumber(account_number));
    }
}
