package com.elotech.app.dto;

import lombok.*;
import org.springframework.data.domain.Sort;

@Builder
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class PersonFilterDTO {
    private int page;
    private int size;
    private String filter;
    private String orderBy;
    private Sort.Direction order;
}
