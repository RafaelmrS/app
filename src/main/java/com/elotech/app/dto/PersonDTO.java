package com.elotech.app.dto;

import com.elotech.app.entity.Person.Address;
import com.elotech.app.entity.Person.ContactPerson;
import com.elotech.app.entity.Person.Person;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Builder
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class PersonDTO {
    @NotNull
    private String name;
    @NotNull
    private String rg;
    @NotNull
    private LocalDate dateOfBirth;
    @NotNull
    private List<ContactPerson> contactPerson;
    @NotNull
    private Address address;

    public static PersonDTO from(Person person) {
        return PersonDTO
                .builder()
                .address(person.getAddress())
                .contactPerson(person.getContactPerson())
                .dateOfBirth(person.getDateOfBirth())
                .name(person.getName())
                .rg(person.getRg())
                .build();
    }
}
