package com.elotech.app.dto;

import com.elotech.app.entity.Person.Address;
import com.sun.istack.NotNull;
import lombok.*;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UpdateAddressDTO {
    @NotNull
    private Address address;
}
