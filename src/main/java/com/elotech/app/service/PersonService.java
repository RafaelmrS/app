package com.elotech.app.service;

import com.elotech.app.dto.PersonDTO;
import com.elotech.app.dto.PersonFilterDTO;
import com.elotech.app.dto.UpdateAddressDTO;
import com.elotech.app.entity.Person.Person;
import org.springframework.data.domain.Page;

public interface PersonService {

    Person getPersonById(Long personId);

    Page<Person> getPagePerson(PersonFilterDTO personFilterDTO);

    void registerPerson(PersonDTO personDTO);

    void updateAddress(Long personId, UpdateAddressDTO updateAddressDTO);

    void deletePerson(Long personId);
}
