package com.elotech.app.service;

import com.elotech.app.entity.BankAccount;
import com.elotech.app.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public BankAccount buscaContaPeloAccountNumber(String accountNumber) {
        BankAccount bankAccountRetornada = bankAccountRepository.findByAccountNumber(accountNumber);

        return bankAccountRetornada;
    }
}
