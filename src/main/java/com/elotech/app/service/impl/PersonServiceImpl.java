package com.elotech.app.service.impl;

import com.elotech.app.dto.PersonDTO;
import com.elotech.app.dto.PersonFilterDTO;
import com.elotech.app.dto.UpdateAddressDTO;
import com.elotech.app.entity.Person.Person;
import com.elotech.app.repository.PersonRepository;
import com.elotech.app.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class PersonServiceImpl implements PersonService {
    private PersonRepository personRepository;

    @Override
    public Person getPersonById(Long personId) {
        return personRepository.getPersonById(personId);
    }

    @Override
    public Page<Person> getPagePerson(PersonFilterDTO personFilterDTO) {
        return personRepository.getPagePerson(personFilterDTO);
    }

    @Override
    public void registerPerson(PersonDTO personDTO) {
        final var person = Person
                .builder()
                .name(personDTO.getName())
                .rg(personDTO.getRg())
                .dateOfBirth(personDTO.getDateOfBirth())
                .address(personDTO.getAddress())
                .contactPerson(personDTO.getContactPerson())
                .build();
        personRepository.savePerson(person);
    }

    @Override
    public void updateAddress(Long personId, UpdateAddressDTO updateAddressDTO) {
        final var person = personRepository.getPersonById(personId);
        person.updateAddress(updateAddressDTO);
        personRepository.savePerson(person);
    }

    @Override
    public void deletePerson(Long personId) {
        personRepository.deletePerson(personId);
    }


}
