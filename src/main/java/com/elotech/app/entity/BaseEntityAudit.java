package com.elotech.app.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public abstract class BaseEntityAudit extends BaseEntity implements Serializable {
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    @PrePersist
    public void buildCreatedAt() {
        version = 0;
        createdAt = LocalDateTime.now();
    }

    @PreUpdate
    public void buildUpdatedAt() {
        updatedAt = LocalDateTime.now();
    }
}
