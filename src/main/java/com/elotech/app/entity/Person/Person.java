package com.elotech.app.entity.Person;

import com.elotech.app.dto.UpdateAddressDTO;
import com.elotech.app.entity.BaseEntityAudit;
import com.elotech.app.entity.BankAccount;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "person")
@Table(name = "person")
@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Where(clause = "removed = false")
public class Person extends BaseEntityAudit implements Serializable {
    private static final long serialVersionUID = 4463062150355428154L;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "rg", length = 10, nullable = false, unique = true)
    private String rg;

    @Column(name = "date_of_birth", nullable = false)
    private LocalDate dateOfBirth;

    @EqualsAndHashCode.Exclude
    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "person",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Builder.Default
    private List<ContactPerson> contactPerson = new ArrayList<>();

    @OneToOne(cascade=CascadeType.ALL)
    private Address address;

    @OneToOne(cascade = CascadeType.ALL)
    private BankAccount bankAccount;

    public void updateAddress(UpdateAddressDTO updateAddressDTO) {
        address = updateAddressDTO.getAddress();
    }
}
