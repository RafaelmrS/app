package com.elotech.app.entity.Person;

import com.elotech.app.entity.BaseEntityAudit;
import lombok.*;

import javax.persistence.*;

@Getter
@Entity(name = "contact_person")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
public class ContactPerson extends BaseEntityAudit {

    @EqualsAndHashCode.Exclude
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    private Person person;

    @Column(length = 11)
    private String phone;

    @Column(name = "email", nullable = false)
    private String email;
}
