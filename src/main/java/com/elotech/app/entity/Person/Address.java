package com.elotech.app.entity.Person;

import com.elotech.app.entity.BaseEntityAudit;
import com.elotech.app.enumeration.TypeResidence;
import lombok.*;

import javax.persistence.*;

@Getter
@Entity(name = "address")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
public class Address extends BaseEntityAudit {

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "neighborhood", nullable = false)
    private String neighborhood;

    @Column(name = "number", nullable = false)
    private Integer number;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_residence")
    private TypeResidence typeResidence;

    @OneToOne(cascade=CascadeType.ALL)
    private Person person;
}
