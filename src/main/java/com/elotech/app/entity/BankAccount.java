package com.elotech.app.entity;


import com.elotech.app.entity.Person.Person;
import com.elotech.app.enumeration.TypeAccount;
import com.elotech.app.enumeration.TypeResidence;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "BankAccount")
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 3468296479994314122L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "account_number", nullable = false)
    private String accountNumber;

    @Column(name = "agency", nullable = false)
    private String agency;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_account", nullable = false)
    private TypeAccount typeAccount;

    @Column(name = "description", nullable = false)
    private String description;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn
    private Person person;
}
